import datetime
from flask import url_for
from app import db

class PlaceForRent(db.Document):
    created_at = db.DateTimeField(default=datetime.datetime.now, required=True)
    address=db.ReferenceField('Address')
    price=db.IntField()
    num_rooms=db.IntField()
    num_bathrooms=db.IntField()
    sqft=db.DecimalField(precision=1)
    unit_number=db.StringField(max_length=255, default="")

    def get_absolute_url(self):
        return url_for('place', kwargs={"id": self.id})

    def __unicode__(self):
        if(self.address):
            return self.address.block_number+" "+self.address.street_name
        else:
            return self.unit_number

    meta = {
            'allow_inheritance': True,
            }

# Reuse the address for similar blocks, using postal code as the primary key
class Address(db.Document):
    block_number=db.StringField(max_length=255)
    street_name=db.StringField(max_length=255, required=True)
    postal_code=db.StringField(primary_key=True, max_length=255, required=True)
    city=db.StringField(max_length=255)
    country=db.StringField(max_length=255)
    coordinates=db.StringField(max_length=255)

    def __unicode__(self):
        return self.block_number+" "+self.street_name+" "+self.postal_code


#address=Address(block_number="546",street_name="Woodlands Drive 16",postal_code="730546",city="Singapore",country="Singapore")
#place=PlaceForRent(price=2000,num_rooms=3,num_bathrooms=2,sqft=108.5,unit_number="#10-31")
