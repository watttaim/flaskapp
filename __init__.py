from flask import Flask
from flask import make_response, jsonify
from flask.ext import restful
from flask.ext.mongoengine import MongoEngine
from mongoengine import connect
app = Flask(__name__)
api = restful.Api(app)

DB_NAME = 'sampledb'
DB_USERNAME = 'taimin'
DB_PASSWORD = 'password'
DB_HOST_ADDRESS = 'ds035270.mongolab.com:35270/sampledb'

app.config["MONGODB_DB"] = DB_NAME
#app.config['MONGODB_SETTINGS'] = {'db':"testdb"}
app.config["SECRET_KEY"] = "KeepThisS3cr3t"
connect(DB_NAME, host='mongodb://' + DB_USERNAME + ':' + DB_PASSWORD + '@' + DB_HOST_ADDRESS)
db = MongoEngine(app)

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify( { 'error': 'Unable to process request' } ), 404)

def register_blueprints(app):
    # Prevents circular imports
    from app.views import places
    from app.api import place_api
    app.register_blueprint(places)
    app.register_blueprint(place_api)

register_blueprints(app)

if __name__ == '__main__':
    import os
    port = int(os.environ.get('PORT', 5000))
    app.run(debug=True, port=port)
