angular
    .module('PlaceApp', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: '../static/place.html',
                controller: 'MainController'
            })
            .when('/create', {
                templateUrl: '../static/add_place.html',
                controller: 'AddController'
            })
            .when('/edit/:place_id', {
                templateUrl: '../static/edit_place.html',
                controller: 'EditController'
            })
            .otherwise({ redirectTo: '/' });
    }])
    .factory('windowAlert', [
        '$window',
        function($window) {
            return $window.alert;
        }
    ])
    .controller('MainController', [
        '$scope',
        '$http',
        'windowAlert',
        '$location',
        function($scope, $http, windowAlert, $location) {
            $scope.RETRIEVE_DEFAULT_NR = 5;
            $scope.state = {};
            $scope.state.placeList = [];
            $scope.state.retrieveNr = $scope.RETRIEVE_DEFAULT_NR;
            $scope.state.pageName = 'placeList';
            $scope.state.searchStreetName='';
            $scope.state.searchPostalCode='';
            $scope.state.searchMinPrice='';
            $scope.state.searchMaxPrice='';

            $scope.searchItem = function() {
                $("div.spinner").show();
                $("tr.place_list").hide();
                    $http
                        .get('/api/v1/places?street_name='+$scope.state.searchStreetName+'&postal_code='+$scope.state.searchPostalCode+
                            '&min_price='+$scope.state.searchMinPrice+
                            '&max_price='+$scope.state.searchMaxPrice)
                        .success(function(data, status, headers, config) {
                            if (data.success) {
                                $("div.spinner").hide();
                                $scope.state.placeList = data.places;
                            } else {
                                windowAlert('Searching of place failed');
                            }
                        })
                        .error(function(data, status, headers, config) {
                        });
            };

            $scope.deletePlace = function(place_id) {
                    $http
                        .delete('/api/v1/places/'+place_id, {
                            place_id: place_id
                        })
                        .success(function(data, status, headers, config) {
                            if (data.success) {
                                $location.path('../static/place.html');
                            } else {
                                windowAlert('Deleting of place failed');
                            }
                        })
                        .error(function(data, status, headers, config) {
                            windowAlert("Deleting of place failed");
                        });
            };

            $scope.retrievePlaces = function() {
                $http
                    .get('/api/v1/places')
                    .success(function(data, status, headers, config) {
                        if (data.success) {
                            $("div.spinner").hide();
                            $scope.state.placeList = data.places;
                        } else {
                            windowAlert('Retrieval failed');
                        }
                    })
                    .error(function(data, status, headers, config) {
                        windowAlert("Retrieval failed");
                    });
            };

            $scope.$on('$viewContentLoaded', function() {
                //call it here
                $scope.retrievePlaces();
            });
        }
    ])
    .controller('AddController', [
        '$scope',
        '$http',
        'windowAlert',
        '$location',
        function($scope,$http,windowAlert,$location) {
            $scope.state = {};
            $scope.state.pageName = 'AddPage';

            $scope.$on('$viewContentLoaded', function() {
                //call it here
                $("ul.nav.secondary-nav").hide();
            });

            $scope.addPlace = function() {
                if (!$scope.state.street_name) {
                    windowAlert("text field must be non-empty");
                } else {
                    $http
                        .post('/api/v1/places', {
                            block_number: $scope.state.block_number,
                            street_name: $scope.state.street_name,
                            unit_number: $scope.state.unit_number,
                            postal_code: $scope.state.postal_code,
                            city: $scope.state.city,
                            country: $scope.state.country,
                            price: $scope.state.price,
                            num_rooms: $scope.state.num_rooms,
                            num_bathrooms: $scope.state.num_bathrooms,
                            sqft: $scope.state.sqft,
                            coordinates: $scope.state.coordinates
                        })
                        .success(function(data, status, headers, config) {
                            if (data.success) {
                                $("ul.nav.secondary-nav").show();
                                $location.path('../static/place.html');
                            } else {
                                windowAlert('Adding of place failed');
                            }
                        })
                        .error(function(data, status, headers, config) {
                            windowAlert('Adding of place failed:'+data.message);
                        });
                }
            };

        }
    ])
.controller('EditController', [
        '$scope',
        '$http',
        'windowAlert',
        '$location',
        '$route',
        function($scope,$http,windowAlert,$location,$route, $routeParams) {
            $scope.state = {};
            $scope.state.place = {};
            $scope.state.pageName = 'EditPage';
            $scope.$route = $route;
            $scope.$routeParams = $routeParams;
            place_id=$scope.$route.current.params['place_id'];
            $http
                .get('/api/v1/places/'+place_id)
                .success(function(data, status, headers, config) {
                    if (data.success) {
                        $scope.state.place=data.place;
                        $("div.spinner").hide();
                        $("form.edit_form").show();
                    } else {
                        windowAlert('Fetching of place failed');
                    }
                })
                .error(function(data, status, headers, config) {
                    windowAlert("Fetching of place failed");
                });

            $scope.$on('$viewContentLoaded', function() {
                //hide the create button
                $("ul.nav.secondary-nav").hide();
                $("form.edit_form").hide();
            });

            $scope.fetchPlace = function() {
                    place_id=$scope.$route.current.params['place_id'];
                    $http
                        .get('/api/v1/places/'+place_id)
                        .success(function(data, status, headers, config) {
                            if (data.success) {
                                $scope.state.place=data.place;
                                windowAlert('Fetching of place succeed');
                            } else {
                                windowAlert('Fetching of place failed');
                            }
                        })
                        .error(function(data, status, headers, config) {
                            windowAlert("Fetching of place failed");
                        });
            };

            $scope.editPlace = function() {
                    place_id=$scope.$route.current.params['place_id'];
                    $http
                        .put('/api/v1/places/'+place_id, {
                            block_number: $scope.state.place.address.block_number,
                            street_name: $scope.state.place.address.street_name,
                            unit_number: $scope.state.place.unit_number,
                            postal_code: $scope.state.place.address.postal_code,
                            city: $scope.state.place.address.city,
                            country: $scope.state.place.address.country,
                            price: $scope.state.place.price,
                            num_rooms: $scope.state.place.num_rooms,
                            num_bathrooms: $scope.state.place.num_bathrooms,
                            sqft: $scope.state.place.sqft,
                            coordinates: $scope.state.place.address.coordinates
                        })
                        .success(function(data, status, headers, config) {
                            if (data.success) {
                                windowAlert('Updating of place succeed');
                            } else {
                                windowAlert('Updating of place failed');
                            }
                        })
                        .error(function(data, status, headers, config) {
                            windowAlert("Updating of place failed");
                        });
            };
        }
    ])
    ;
