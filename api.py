from app import api
from flask import make_response, jsonify, Blueprint, request
from flask.ext.restful import Resource, reqparse
from app.models import PlaceForRent, Address
from mongoengine.queryset import Q

place_api = Blueprint('place_api', __name__)

parser = reqparse.RequestParser()
parser.add_argument('price', type=int, required=True)
parser.add_argument('num_rooms', type=int, required=True)
parser.add_argument('num_bathrooms', type=int, required=True)
parser.add_argument('sqft', type=float, required=True)
parser.add_argument('unit_number', type=str)
parser.add_argument('block_number', type=str)
parser.add_argument('street_name', type=str, required=True)
parser.add_argument('postal_code', type=str, required=True)
parser.add_argument('city', type=str)
parser.add_argument('country', type=str)
parser.add_argument('coordinates', type=str)

class PlaceListForRentAPI(Resource):
    def get(self):
        # Because the address is a referenced field and mongo do not have join,
        # we search the addresses first
        if('street_name' in request.args):
            street_name=request.args.get('street_name')
            postal_code=request.args.get('postal_code')
            maxPrice=request.args.get('max_price')
            minPrice=request.args.get('min_price')
            if(len(street_name) > 0 and len(postal_code) > 0):
                addresses=Address.objects.filter(Q(street_name__icontains=street_name) & Q(postal_code__icontains=postal_code))
            elif(len(street_name) == 0 and len(postal_code) > 0):
                addresses=Address.objects.filter(postal_code__icontains=postal_code)
            else:
                addresses=Address.objects.filter(street_name__icontains=street_name)

            if(len(maxPrice) == 0 and len(minPrice) == 0):
                places=PlaceForRent.objects.filter(address__in=addresses)
            elif(len(maxPrice) > 0 and len(minPrice) == 0):
                places=PlaceForRent.objects.filter(address__in=addresses).filter(price__lte=int(maxPrice))
            elif(len(maxPrice) == 0 and len(minPrice) > 0):
                places=PlaceForRent.objects.filter(address__in=addresses).filter(price__gte=int(minPrice))
            else:
                places=PlaceForRent.objects.filter(address__in=addresses).filter(price__lte=int(maxPrice)).filter(price__gte=int(minPrice))
        else:
            places=PlaceForRent.objects.all()
        new_places=[]
        for place in places:
            address = Address.objects.get(postal_code=place.address.id)
            new_address={}
            new_address['block_number']=address.block_number
            new_address['street_name']=address.street_name
            new_address['postal_code']=address.postal_code
            new_address['city']=address.city
            new_address['country']=address.country
            new_address['coordinates']=address.coordinates
            place.address=new_address
            new_places.append(place)
        return make_response(jsonify({'success':True, 'places': new_places, 'args':request.args.get('street_name')}), 200)

    def post(self):
        # create a new place
        args = parser.parse_args()
        place=PlaceForRent()
        try:
            address=Address.objects.get(postal_code=args['postal_code'])
        except:
            address=Address(postal_code=args['postal_code'])
        address.block_number=args['block_number']
        address.street_name=args['street_name']
        address.postal_code=args['postal_code']
        address.city=args['city']
        address.country=args['country']
        address.coordinates=args['coordinates']
        address.save()
        place.price=args['price']
        place.num_rooms=args['num_rooms']
        place.num_bathrooms=args['num_bathrooms']
        place.sqft=args['sqft']
        place.unit_number=args['unit_number']
        place.address=address
        place.save()
        return make_response(jsonify({'success':True, 'place': place}), 201)

class PlaceForRentAPI(Resource):
    def get(self, place_id):
        # Fetch a single place, used by edit function to populate existing data
        place=PlaceForRent.objects.get_or_404(id=place_id)
        address = Address.objects.get(postal_code=place.address.id)
        new_address={}
        new_address['block_number']=address.block_number
        new_address['street_name']=address.street_name
        new_address['postal_code']=address.postal_code
        new_address['city']=address.city
        new_address['country']=address.country
        new_address['coordinates']=address.coordinates
        place.address=new_address
        return make_response(jsonify({'success':True, 'place':place}), 200)

    def delete(self, place_id):
        # delete a single place
        place=PlaceForRent.objects.get_or_404(id=place_id)
        place.delete();
        return make_response(jsonify({'success':True}), 200)

    def put(self, place_id):
        # update a single place
        args = parser.parse_args()
        try:
            address=Address.objects.get(postal_code=args['postal_code'])
        except:
            address=Address(postal_code=args['postal_code'])
        address.block_number=args['block_number']
        address.street_name=args['street_name']
        address.postal_code=args['postal_code']
        address.city=args['city']
        address.country=args['country']
        address.coordinates=args['coordinates']
        address.save()
        place=PlaceForRent.objects.get_or_404(id=place_id)
        place.price=args['price']
        place.num_rooms=args['num_rooms']
        place.num_bathrooms=args['num_bathrooms']
        place.sqft=args['sqft']
        place.unit_number=args['unit_number']
        place.save()
        return make_response(jsonify({'success':True}), 200)
        pass

# Not sure how i can combine these 2 urls into 1 class
api.add_resource(PlaceListForRentAPI, '/api/v1/places')
api.add_resource(PlaceForRentAPI, '/api/v1/places/<string:place_id>')
