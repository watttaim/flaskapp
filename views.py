#from app import app
from flask import Blueprint, request, redirect, render_template, url_for
from flask.ext.mongoengine.wtf import model_form
from flask.views import MethodView
from app.models import PlaceForRent, Address


places = Blueprint('places', __name__, template_folder='templates')


class ListView(MethodView):

    def get(self):
        places = PlaceForRent.objects.all()
        return render_template('places/list.html', places=places)


class DetailView(MethodView):

    #form = model_form(PlaceForRent, exclude=['created_at'])

    def get_context(self, place_id=None):
        #place = PlaceForRent.objects.get_or_404(id=place_id)
        #form = self.form(request.form)

        #context = {
        #    "place": place,
        #    "form": form
        #}
        #return context

        form_cls = model_form(PlaceForRent, exclude=['created_at'])
        form_cls.address.label_attr='descrizione'

        if place_id:
            place = PlaceForRent.objects.get_or_404(id=place_id)
            if request.method == 'POST':
                form = form_cls(request.form, inital=place._data)
            else:
                form = form_cls(obj=place)
        else:
            place = PlaceForRent()
            form = form_cls(request.form)

        context = {
            "place": place,
            "form": form,
            "create": place_id is None
        }
        return context

    def get(self, place_id):
        #place = PlaceForRent.objects.get_or_404(id=place_id)
        if('delete' in request.path):
            print "HIHI"
            return self.delete(place_id)
        else:
            context = self.get_context(place_id)
            #return render_template('places/details.html', place=place)
            return render_template('places/details.html', **context)

    def post(self, place_id):
        context = self.get_context(place_id)
        form = context.get('form')
        form.address.label_attr='descrizione'

        if form.validate():
            place = context.get('place')
            form.populate_obj(place)
            place.save()

            return redirect(url_for('places.list'))

        return render_template('places/list.html', **context)

    def delete(self, place_id):
        context = self.get_context(place_id)
        place = context.get('place')
        place.delete()
        return redirect(url_for('places.list'))

# Register the urls
places.add_url_rule('/', view_func=ListView.as_view('list'))
places.add_url_rule('/<place_id>/', view_func=DetailView.as_view('detail'))
#places.add_url_rule('/create/', defaults={'place_id': None}, view_func=DetailView.as_view('create'))
places.add_url_rule('/edit/<place_id>/', view_func=DetailView.as_view('edit'))
places.add_url_rule('/delete/<place_id>/', view_func=DetailView.as_view('delete'))
